# TP 

Les exercices du TP ont pour but de vous apprendre a utiliser Docker. Les réponses n'étant pas toutes dans le cours, je vous invite a consulter la documentation officielle. Lien vers la doc générale : [Documentation](https://docs.docker.com/engine/reference/run/)

# Partie 1 : Les bases
### Le run du premier container
Lancer un container avec l'image nginx:latest, et lui donner le nom __nginx__ . Voici la commande a lancer pour cela : 

```bash
docker run --name nginx nginx:latest
```

Que constastez vous sur le terminal ? Pouvez-vous faire une commande ? 

Pour quitter ça et reprendre la main sur votre terminal vous devez interrompre le processus en cours avec un SIGINT. Vous avez maintenant quiter le container. Faites la commande suivante pour lister les containers pour voir l'état de ce dernier. 

```bash
docker ps
```

Le voyez vous et pourquoi ?

Pour éviter de perdre la main sur notre terminal, vous allez lancer le container en mode détacher avec l'option ```-d```. 

```bash
docker run -d --name nginx nginx:latest
```

Que constatez vous et pourquoi ? 

Il y a plusieurs solutions possibles, trouvez en une. L'objectif est de pourvoir lancer la commande ```docker ps``` avec un container en cours d'exécution sur la machine ET conserver la main sur votre terminal.

Quelle(s) commande(s) avez vous effectuée(s) pour cela ? 

### Connexion à un container en cours de fonctionnement

Maintenant que nous avons un container en cours de fonctionnement nous allons voir comment se connecter à l'intérieur pour faire divers vérifications et opérations.

Dans un premier temps vous allez effectuer un __curl__ du container dans le but d'obtenir la page HTML par défaut de nginx.

Pour pouvoir utiliser la commande curl il faut dans un premier récupérer l'IP du container. Pour cela il faut effectuer la commande ```docker inspect <NOM-CONTAINER>```, et trouver quel est l'IP de ce dernier. 

Une fois cette information trouvé, vous allez effectuer la commande ```curl <IP-CONTAINER>```. 
Le résultat devrait ressembler à quelque chose comme ça : 

![Expose](images/curl.png)

Maintenant vous allez vous connecter dans le container pour modifier le fichier __index.html__ de ce dernier avec le contenu de votre choix.
Pour cela il faut effectuer la commande suivante : 

```bash
docker exec -it <NOM-CONTAINER> bash 
```

Si vous voyez que le nom d'hôte de votre machine a changer pour un hash, c'est que vous êtes bien dans le container, pour vous en assurer, vous pouvez effectuer la commande ```hostname```. 

Vous allez maintenant modifier le ficher __index.html__. (Petit indice, le fichier se trouve dans __/usr/share/nginx/html/__). 

Que se passe-t-il si vous utilisez __vi__ pour modifier le fichier ? Pourquoi ce problème survient-il et comment pouvez vous le régler ? 

### L'ouverture de port

L'exercice précédent nous a montré comment créer un container, et se connecter dessus, l'objectif va maintenant être d'exposer ce container pour qu'il puisse être accéssible depuis l'extérieur. 

Pour cela vous devez rajouter l'option ```-p <PORT-MACHINE>:<PORT-CONTAINER>``` dans la commande ```docker run```

Voici la commande à effectuer : 

```bash
docker run -d -p 80:80 nginx:latest
```

Maintenant rendez-vous sur votre navigateur et tapez l'ip de votre VM dans la barre d'URL.
Vous devez obtenir un résultat ressemblant à ça.

![Expose](images/expose.png)

Vous avez exposé votre container, félicitations. 

Maintenant vous allez effectuer la commande suivante :

```bash
docker run -d -p 80:8080 nginx:latest
```

Que se passe-t-il et pourquoi ? (détaillez les actions que vous avez effectuées pour résoudre le problème)

Une fois que vous aurait répondu à la question d'avant vous pouvez continuer et effetuer la commande suivante : 

```bash
docker run -d -p 8080:8080 nginx:latest
```

Que devez vous taper dans votre navigateur pour accéder au container ? Est-ce que cela fonctionne ? Pourquoi ?

Pour trouver la réponse à cette question, comme vous n'êtes pas l'auteur de cette image Docker je vous invite à taper la commande suivante : 

```bash
docker image inspect nginx:latest
```

Une fois que vous avez compris d'ou vient le problème, lancez un container nginx sur votre machine oouvert sur le port 8080 de votre VM. Quelle commande avez vous effectuée pour cela ? 

## Exercice 1 

L'objectif de cet exercice va maintenant être de lancer 2 containers, un exposé sur le port 8080, et un exposé sur le port 8081. Vous aller de plus modifier le contenu des fichiers __index.html__ DES 2 containers, avec le contenu de votre choix MAIS, différents dans les deux.


# Partie 2 : Le build 

### Recherche et téléchargement d'image sur le Hub

Le [docker hub](https://hub.docker.com/) recense énormément d’images disponibles en générale
gratuites. Avant d’installer une application, je vous conseille d’aller y faire un tour, vous vous rendrez
compte de la popularité de docker et du temps que vous allez (auriez ?) gagner. Rendez vous dans la partie __explore__ pour pouvoir parcourir les images disponibles.

Qu'est ce que __alpine__, l'image la plus populaire du Hub ?

Récupérez la dernière version de l'image alpine et l'image debian/ubuntu de votre choix.

Quand vous récupérez une image, son nom s'affiche de la manière suivante <NAME<NAME>>:<TAG<TAG>>.

Que veut dire le tag "latest", est-ce une bonne ou une mauvaise pratique de l'utiliser et pourquoi ?

### Créer son premier Dockerfile et builder son image

Comme expliqué précédement dans le README, le Dockerfile est la base d'une image Docker, c'est à partir de ce fichier que va être créé l'image Docker qui servira à instancier des containers. Le Dockerfile doit contenir obligatoirement 2 instructions, la première va être le FROM, qui va nous donner l'image à partir de laquelle notre image va se construire. (Il existe la possibilité de faire son image "from scratch", mais je déconseille car il faut un assez bon niveua en linux, si celà vous intéresse voici le lien de la doc mais ce n'est __PAS__ obligatoire. [Image de base](https://docs.docker.com/build/building/base-images/)). Et le CMD et/ou ENTRYPOINT, qui est la commande que va lancer notre container à son lancement. 

> :warning:
>
> Un container ne va rester actif que si l'instuction qu'il lance via le CMD/ENTRYPOINT s'exécute en permanence. 
> 
>Exemple : Si vous mettez l'instruction ```CMD ["echo","coucou"]``` dans votre Dockerfile, au lancement de votre container, le container va exécuter la commande ```echo coucou```, ce qui va afficher "coucou" sur votre terminal et votre container va se terminer, il a fait ce pourquoi il avait été programmé, exécuter la commande "echo", une fois l'objectif atteint il n'a plus de raison de continuer son exécution. 
> 
>C'est pour cela que pour que votre container tourne correctement il faut que vous utilisiez une commande "infinie" (qui ne rend la main que quand il se produit un erreur ou quand elle est stoppée avec un SIGNINT)
>
> Exemple : L'image nginx officielle disponible sur le Docker Hub utilise la commande suivante en CMD ```["nginx","-g","daemon off;]```. La façon standard de lancer nginx sur une machine linux (debian) est de faire la commande suivante ```service nginx start```, le problème c'est que cette commande active le service nginx, mais qu'elle nous rend la main une fois que le service est lancé. La commande utilisée par l'image nginx officielle lance le service et affiche les logs de celui-ci dans le terminal, de ce fait le container exécute un processus permanent qui ne s'arrête qu'en cas d'erreur ou d'un arrêt forcé du processus. 

Et entre le FROM et le CMD/ENTRYPOINT vous allez rajouter des instructions pour rajouter des fichier, installer des packages, créer des users, etc... pour que votre image puisse réponde à votre besoin. voici la liste des arguments qui peut être utilisé dans votre Dockerfile : 

- FROM : Définit l'image de base qui sera utilisée par les instructions suivantes.
- LABEL : Ajoute des métadonnées à l'image avec un système de clés-valeurs, permet par
exemple d'indiquer à l'utilisateur, l'auteur du Dockerfile.
- ARG : Variables temporaires qu'on peut utiliser dans un Dockerfile.
- ENV : Variables d'environnements utilisables dans votre Dockerfile et transmise au
conteneur.
- USER : Désigne quel est l'utilisateur qui lancera les prochaines instructions RUN, CMD ou
ENTRYPOINT (par défaut c'est l'utilisateur root).
- WORKDIR : Définit le répertoire de travail qui sera utilisé pour le lancement des
commandes CMD et/ou ENTRYPOINT et ça sera aussi le dossier courant lors du
démarrage du conteneur.
- RUN : Exécute des commandes Linux ou Windows lors de la création de l'image. Chaque
instruction RUN va créer une couche en cache qui sera réutilisée dans le cas de
modification ultérieure du Dockerfile.
- COPY : Permet de copier des fichiers depuis notre machine locale vers le conteneur
Docker.
- EXPOSE : Expose un port.
- CMD : Exécute une commande par défaut si rien n’est précisé au lancement du
conteneur. Spécifie les arguments qui seront envoyés au ENTRYPOINT, (on peut aussi
l'utiliser pour lancer des commandes par défaut lors du démarrage d'un conteneur). S’il
est utilisé pour fournir des arguments par défaut pour l'instruction ENTRYPOINT, alors les
instructions CMD et ENTRYPOINT doivent être spécifiées au format de tableau JSON.
- ENTRYPOINT : comme son nom l'indique, c'est le point d'entrée de votre conteneur, en
d'autres termes, c'est la commande qui sera toujours exécutée au démarrage du
conteneur. (par defaut c’est /bin/sh -c, c’est-à-dire qu’un shell est exécuté avec la
possibilité d’y ajouter une commande)

## Exercice 2

Maintenant que vous savez comment fonctionne un Dockerfile, vous allez créer votre premier Dockerfile qui sera l'équivalent de l'image "hello-world", c'est à dire une image qui à son démarrage va afficher du texte sur le terminal puis se terminer. 

Pour cela vous allez récupérer le "dessin" de votre choix sur le site [asciiart](https://www.asciiart.eu/) et vous l'affichez au démarrage du container (c'est à dire dans l'instruction CMD). Pour que cela soit plus facile vous allez dans un premier temps réaliser cela avec l'image ubuntu/debian que vous avez téléchargé dans l'exercice précédent. 

Petite astuce : 2 solutions s'offrent à vous pour réaliser ce Dockerfile, la première est de télécharger l'image dans le Dockerfile grâce à une commande, la seconde est de télécharger l'image sur votre hôte Docker et de copier le ficher dans le Dockerfile. 

Une est solution est meilleure que l'autre, pouvez vous me dire laquelle et pourquoi selon vous? 

### Le ENTRYPOINT et le CMD

Pour bien comprendre la différence entre les deux nous allons faire quelques tests. Comme nous l'avons vu précédemment, il existe deux directives Dockerfile pour spécifier quelle commande exécuter par défaut dans les images construites. Si vous spécifiez uniquement CMD alors docker exécutera cette commande en utilisant le ENTRYPOINT par défaut, à savoir /bin/sh -c . Vous pouvez remplacer soit le point d’entrée, soit la commande lorsque vous démarrez l’image construite. Si vous spécifiez les deux, alors ENTRYPOINT spécifie l'exécutable de votre processus de conteneur et CMD sera fourni comme
paramètre de cet exécutable.

Par exemple, si votre Dockerfile contient :

```
FROM ubuntu:16.04
CMD ["date"]
```

Vous utilisez ensuite la directive ENTRYPOINT (non précisée dans le DOckerfile explicitement) par défaut de /bin/sh -c ce qui donnera la commande de votre processus de conteneur sera /bin/sh -c /bin/date .

Une fois que vous exécutez cette image, elle affichera par défaut la date actuelle.

```bash
docker build -t test .
docker run test
```
![Date](images/date.png)

Vous pouvez remplacer CMD sur la ligne de commande, auquel cas il exécutera la commande que vous avez spécifiée.

```bash
docker run test hostname
```
![Hostname](images/hostname.png)

Si vous spécifiez une directive ENTRYPOINT, Docker utilisera cet exécutable et la directive CMD spécifie le ou les paramètres par défaut de la commande. Donc, si votre Dockerfile contient :

```
FROM ubuntu:16.04
ENTRYPOINT ["/bin/echo"]
CMD ["date"]
```

Ce qui donnera :

```bash
docker build -t test2 .
docker run test2
```
![Echo](images/echo.png)

Et comme précédemment vous pouvez remplacer la valeur du CMD, mais cette fois ci, le CMD n'étant que l'argument du ENTRYPOINT, cela ne changera que le message que la commande "echo" va exécuter et pas la commande elle même comme la fois d'avant. 

```bash
docker run test hello_world
```

![Override Echo](images/helloworld.png)

### Encore des Dockerfile

Nous allons maintenant voir comment installer des packages dans notre Dockerfile. 

## Exercice 3

Vous allez créer une image basique via un Dockerfile qui affichera une vache disant coucou (paquet cowsay pour afficher la vache + paquet figlet pour la police spéciale) à l’exécution du docker. Testez votre image en l’instanciant. Partez d’une image basique ubuntu/debian. Je vous invite fortement à aller lire la documentation pour ces deux packages.

Une fois l'image créée et opérationnelle vous allez push votre image sur le Dockerhub, pour cela il faut que vous rendiez sur le site du hub et que vous vous créiez un compte.

Il faut le compte créé il faut vous authentifier sur votre hôte Docker, puis push votre image.

Petite astuce : vous ne pouvez push votre image sur le Dockerhub qu'avec le format suivant : <USERNAME>/<IMAGE-NAME>:<TAG>, donc pour cela il faut soit : qu'au build de votre image, vous donniez le nom avec ce votre à celle-ci, soit que vous taggiez l'image une deuxième fois. 
Exemple : Mon pseudo est Toto sur le Dockerhub et je veux push mon image s'appelle cowsay, un des tags de mon image doit être ```toto/cowsay:v1```. 


### Dockerfile avancé

Jusqu'a présent nous avons vu comment faire un Dockerfile assez basique. Nous allons maintenant voir comment faire des Dockerfile un peu plus compliqué ainsi que certaines bonnes pratiques à avoir lors de la création des images Docker, que ce soit pour les __Dev__ ou les __Infra__. 

## Exercice 4

Cet exercice va consister :
- Pour les devs, a créer un environnement de développement dans un container, l'objectif de ça est premièrement d'avoir un environnement de travail portatif, de ne rien installer sur sa machine (surtout si c'est une machine windows). Et dans un contexte professionnel cela peut servir à onboarder des nouveaux membres à notre équipe plus rapidement. 
- Pour les infras, à créer un serveur web capable d'exécuter du PHP

#### Dev

Pour ce cas là nous allons utiliser un projet Angular, vous pouvez récupérer le projet [ici](https://github.com/Killbzz/demo-angular). Pour parvenir à notre objectif il va falloir effectuer les actions suivantes dans votre Dockerfile :

- Récupérer le projet sur git
- Utiliser une image contenant les binaires nécessaire pour compiler l'application (Les infos sur comment build l'application est dans le README du projet)
- Build le projet à partir du code 
- Exposer les ports de l'application 
- Lancer l'application au démarrage du container

#### Infra

Pour les infra vous allez créer un serveur web apache ou nginx (ceci est votre choix) que vous allez conteneurisé, et il doit être capable d'interpréter du PHP.
Vous pouvez eécupérer le code PHP [ici](https://github.com/Killbzz/demo-php). Et ensuite voici les étapes à suivre : 

- Utiliser une image d'OS de base (debian/ubuntu)
- Récupérer le projet sur git (Utiliser une image qui contient la CLI git ou bien l'installer)
- Installer les packages nécessaire pour exécuter du code PHP
- Exposer les ports de l'application
- Lancer le serveur web au démarrage du container

### Optimiser son Dockerfile

Pourquoi optimiser la taille de son Dockerfile ? La question est légitime, pourquoi passer du temps a optimiser nos images ? Il y a 3 raisons principales : 

- La première, plus une image est légère plus elle se lance vite, si vous avez une image de plus de 5GO il faut se poser des questions car c'est presque la taille d'une VM.
- La deuxième est le stockage, dans une entreprise les images sur versionnées dans une registry (Artifactory JFROG, Harbor, etc ...) et peuvent contenir plusieurs dizaines, voir centaines de versions d'une image. Sur une entreprise de taille moyenne avec une centaine de projets cela représente plusieurs TO de stockage, si vous optimisez la taille de vos images, vous réduisez l'espace de stockage nécessaire pour les versionner. (Économie, Green IT, tout ça)
- Et la dernière est que plus une image est petite, moins elle prend de temps à se construire (dans une CI par exemple), ce qui va augmenter la vitesse de production de votre équipe.

Maintenant que vous savez pourquoi le faire, nous allons voir comment on peut l'optimiser avec quelques simple gestes. 
Le premier est de prendre conscience de taille de vos images, pour cela effectuer la commande ```docker images``` ou ```docker image ls``` 
Ensuite, comme dit précédemment, les images Docker sont construitent sous forme de Layers (couches), chaque ligne (instruction) dans votre Dockerfile représente un layer, le but va donc être d'en faire le moins possible. Pour voir cette différence vous allez créer un Dockerfile avec les instruction suivantes : 

```
FROM ubuntu:latest

RUN apt-get update
RUN apt-get install -y git
RUN apt-get install -y curl
RUN apt-get install -y htop

CMD htop
```

Et un autre Dockefile avec toutes les installation dans la même instruction RUN : 

```
FROM ubuntu:latest

RUN apt-get update && apt-get install -y git curl htop

CMD htop
```

Maintenant regardez la taille des deux images. Certes la différence n'est pas grande car il y a assez peu d'instruction. Mais sur un Dockerfile plus important la différence peu être de plusieurs dizaines, voir centaines de MO. 

Maintenant, il faut supprimer les sources lists. Rajoutez la commande ```rm -rf /var/lib/apt/lists/*``` dans le RUN, et buildez l'image. 
Vous venez de gagner environs 50MO sur une images de ~200MO. 
Les sources lists sont des listes contenant les signatures et les hashs des packages linux disponible. Ils sont mis à jour lorsque vous lancez la commande ```apt-get update```. Dans notre cas comme les containers embarquent toutes leurs dépendances, les sources lists ne nous sont plus utilent une fois les packages installés. 
Il existe de plus des distributions linux extêmement légère, utiliser une image alpine peut donc être une idée intéressante.

Maintenant pour optimiser le temps de build nous allons voir un concept un peu plus avancé. Lors du build d'un Dockerfile, le Docker-engine va lire le Dockerfile ligne par ligne et exécuter les instruction de manière séquentielle, le tout en gardant du cache, ce qui veut dire que si vous changez la première ligne du Dockerfile (l'image du FROM par exemple), le build va reconstruire l'image entière. Mais si vous changez uniquement la dernière ligne (le CMD ou ENTRYPOINT), la construction de l'image sera quasi instantané car toutes les instructions précédentes n'ont pas changés, le Docker-engine peut réutiliser les layers précédent. Voilà les explications en ce qui concerne le fonctionnement du build, pour la construction du Dockerfile cela veut dire qu'il faut que vous placiez vos instructions de manière intélligente. Pour le développement, vous allez d'abord copier vos dépendances (librairies, packages, etc...) car elles sont susceptible de ne pas changer à chaque fois, si vous les copier en même temps que votre code, votre image devra télécharger les dépendances à chaque build. Si vous les mettez avant, votre image ne devra télécharger les dépendances que quand elles changent, ce qui sur un build peut vous faire gagner plusieurs minutes. 

Pour les infras c'est la même chose, si vous voulez configurer un service, mettez en premier l'installation des packages, et ensuite la configuration. Cela vous évitera de devoir télécharger de nouveau tous les packages à chaque fois que vous changerai la configuration du service.

#### Bonus pour l'optimisation/bonnes pratiques pour le Dockerfile
Voici une conférence ayant eu lieu à la Devoxx Paris en 2021, qui explique les bonnes pratiques pour faire les Dockerfile [Video](https://www.youtube.com/watch?v=e63MOPOvEwU). 
Pour vous donner du contexte, en 2021 la majorité des grandes et moyennes entreprises ont déjà transitionnées sur Kubernetes, et Docker est encore le moteur de container de celui-ci. Toutes les conférences de cet évènement portait sur K8S. TOUTES. A ce moment il n'y a plus de conférence sur Docker depuis 2016-2017, mais des développeurs de Docker et de Datadog trouvaient qu'il y avait encore tellement de mauvaise pratique sur le build des images qu'ils se sont senti obligés de faire une conférence pendant un des plus gros évènement DevOps de l'année en France, sur ce sujet. 
Tout ça pour dire que c'est très important et qu'avoir les bons gestes et les bonnes pratiques sur Docker ne sont toujours pas d'actualité dans le marché, c'est une compétence encore à l'heure actuelle, assez rare.

## Exercice 5

Utilisez l'image Docker que vous avez créé lors de l'exercice 4 et appliquez les bonne pratique que vous venez de voir pour optimiser la taille de votre image.

# Partie 3 : La persistance 

Comme nous l'avons vu précédemment, il existe 2 type de volume, les volumes montés et les volumes nommés. Les deux ont la même utilité qui est de faire persister les données du container sur la machie hote une fois le container arrêté/supprimé. La principale différence entre les deux est que les volumes nommés sont manageable directement via la CLI de Docker, alors que les volumes montés sont un simple "partage" de stockage entre l'hôte et le container, qui se managera avec des commandes linux standard. 

> :warning:
>
> Les volumes ne peuvent être attachés à un container UNIQUEMENT au lancement de ce dernier.
>

### Container avec un volume monté

Nous allons commencer avec les volumes les plus basiques, les volumes montés. Pour attacher un volume à un container nous allons utilise l'argument ```-v``` suivi du dossier que nous voulons attaché a mon container, et de l'endroit ou je veux que ce dossier soit monté dans mon container. 

Exemple : 

Je veux attacher le dossier "work" de mon utilisateur __toto__ dans le dossier "opt" de mon container. Cela veut dire qu'il y aura une connexion bidirectionnelle entre le dossier __/home/toto/work__ de ma machine hôte, et le dossier __/opt__ de mon container, tout ce que je vais stocker dans le dossier /__/opt__ de mon container apparaitra dans le dossier __/home/toto/work__ de mon hôte et vise versa. 

Vous allez maintenant faire les commandes suivantes : 

```bash
#Placez vous à la racine de votre utilisateur
cd
#Création de notre dossier 
mkdir work
#Lancer le container en remplacant <PATH> par le chemin absolue du dossier work créé avec la commande précédente 
docker run -d -p 80:80 -v <PATH>:/usr/share/nginx/html nginx
```

Maintenant rendez vous sur votre navigateur sur le port 80, que se passe-t-il ?

Pour essayer de comprendre pourquoi, connectez vous dans votre container et allez voir dans le dossier qui est censé contenir les fichiers HTML de Nginx. Que voyez vous ? 

Sans toucher à la configuration du container, ou le redémarrer, trouver un moyen avec vos connaissances de résoudre le problème (résoudre = avoir un code 200 et non plus un 4XX)

### Volume nommé

Maintenant nous allons pouvoir voir les volumes nommés, les "vraies" volumes managé dirctement par Docker. Dans un premier temps nous allons créer un volume grâce à la CLI, en entilisant la commande suivante : 

```bash
docker volume create work
```

Nous avons créé notre premier volume ! Nous allons maintenant relancer la commande que nous avons effectuer avec les volumes montés mais cette voici avec notre nouveau volume (arretez le container précedent si besoin pour libérer le port) : 

```bash
docker run -d -p 80:80 -v work:/usr/share/nginx/html nginx
```

Rendez vous sur votre navigateur. Que constastez vous par rapport à ce qui s'est passé précédemment avec le volume monté ? 

Arretez le container et aller voir ce que contient le volume "work". Pour trouver son emplacement vous pouvez utiliser la commande ```docker volume inspect <VOLUME_NAME>```, cela vous donnera le dossier où sont stockées les données du volume. 

Modifiez le index.html de façon notable, puis relancez un container avec la même commande que la précédente. Allez sur votre navigateur et que constatez vous maintenant ? 

Pouvez vous expliquer avec vos mots la différence de comportement entre les volumes montés et le volumes nommés ?

## Exercice 6

Maintenant que nous avons vu comment fonctionne la persistance des données avec les volumes vous allez réaliser un exercice en fonction de votre spé (Dev,Infra).

#### Dev

Pour les Devs vous allez mettre en place un envrionnement de développement, mais cette fois ci le vous devrez être de capable changer des fichier sans devoir rentrer dans le container. Le but est d'avoir un container contenant tous packages nécessaires au fonctionnement de l'application et de pouvoir développer depuis votre IDE sur votre machine. Réutilisez l'image de l'exercice 4 pour cela. La seule contrainte va être de NE PAS télécharger node sur votre machine (vous utiliserai le node du container pour exécuter votre code). Il y a plusieurs façons de réaliser cet exercice, certaines plus faciles, d'autres plus pratiques, mais elles se valent à peu près, vous n'avez donc pas besoin de ligne directrice pour réaliser cet exercice. 

#### Infra

Pour les Infras vous allez mettre en place la persistance des données sur une BDD. Pour cela vous allez utiliser le script SQL présent sur le projet. 
- La première étape va être de récupérer l'image MySQL sur le DockerHub.
- La deuxième étape va être de trouver le PATH où MySQL stocke ses data dans un container pour monter votre volume dans le bon dossier.
- La troisième étape va être de lancer un container avec le bon type de volume (vous avez 50% de chance mais en réfléchissant un peu vous devriez trouver le bon) et d'importer le script SQL fourni dans le projet. 
- Dernière étape, stoppez et supprimez le container, lancer en un nouveau et vérifier que les data soient toujours présentent.

Bon chance.

# Partie 4 : Le réseau

Docker étant un outil de virtualisation, il implémente des fontionnalités de réseaux. A l'installation de Docker votre machine va rajouter une nouvelle carte réseau virtuelle appellée "docker0" avec l'adresse 172.17.0.1 , c'est l'adresse IP de votre machine dans le réseau Docker par défaut de type bridge. Si vous faites la commande ```docker network ls``` vous pouvez voir que vous avez plusieurs résultat, et une colonne s'appele __DRIVER__, dans notre cas, le driver peut se traduire par "type de réseau". 
A l'inverse des volumes que nous avons vu juste avant, un container peut être attaché à un réseau après sa création. 

### Le default bridge

Le premier réseau de Docker est celui par défaut le default bridge, c'est là que vont être créés tous nos containers Docker si on ne leurs précise rien à leurs création. Pour cela nous allons faire un test. 
Nous allons lancer 3 containers avec des noms bien disctint pour voir ce qu'il se passe au niveau réseau : 

```bash
docker run -itd --name elastic busybox
docker run -itd --name grafana busybox
docker run -itd --name gitlab busybox
```

Maintenant allons vérifier l'IP de chaque container avec la commande ```docker inspect <CONTAINER_NAME>```, vous devez obtenir un résultat similaire à celui-ci : 

![Network inspect](images/network.png)

Une fois que vous aurez récupéré les IP de vos container connectez vous sur un d'entre eux en ligne de commande et pingez les autres. 

### Le user-defined bridge

Le network user-defined bridge marche de la même façon que le default bridge à la différence que cette fois ci c'est vous qui allez définir les paramètre de celui-ci. Pour créer ce network il faut que vous utilisiez la commande suivante : 

```bash
docker network create -d <DRIVER> --subnet <IP_RANGE> <NAME>
#Exemple
docker network create -d bridge --subnet 192.168.2.0/24 my-network2
```

En faisant ```docker network ls``` vous devriez maintenant voir le nouveau réseau que vous avez créé. Nous allons maintenant créer un nouveau container et l'assigner dans ce réseau, pour cela il faut rajouter le paramètre ```--network=<NETWORK_NAME>```

Faites maintenant un ```docker inspect``` sur ce nouveau container, puis connectez vous en CLI dessus. Pouvez vous ping les containers que vous avez créé précédemment ? 

Vous venez d'apprendre a créer des sous-réseau au sein de Docker ! 

Vous pouvez aussi rajouter des paramètre lors de la création du network pour lui préciser la range des IP que docker doit distribuer pour ses containers avec le paramètre ```--ip-range <IP_RANGE>``` exemple : ```--ip-range 192.168.1.128/25``` 

L'avantage majeur du user-defined bridge est son DNS, vous pouvez maintenant ping les container entre eux en utilisant leurs nom et non pas uniquemement leurs IP comme c'était le cas dans le default bridge. 

Lancer 2 nouveaux container avec le nouveau network : 

```bash
docker run -itd --name vmware --network my-network busybox
docker run -itd --name kube --network my-network busybox
```

Si vous rentrez en CLI dans le container kube, vous pouvez maintenant faire un ```ping vmware``` qui via le DNS de Docker ira ping le container vmware.

### Le host network

Le host network quand à lui est un peu différent. Quand un container est déployé sur le network "host", il prend comme carte réseau celui de la machine hote, ce qui veut dire que c'est comme si il devenait un simple service sur la machine. 

Pour faire un test vous allez arreter toutes les application susecptible de tourner sur le port 80, une fois fait vous allez lancer un container nginx sur le network host avec la commande suivante : 

```bash
docker run -d --network host nginx
```

Allez sur votre navigateur. Vous devriez avoir une page d'accueil Nginx, alors que vous n'avez redirigé aucun port vers le container. 

Le réseau host va utiliser la carte réseau de la machine et donc comme le port ouvert sur l'image nginx est le 80, l'application va utiliser le port 80 de notre machine, sans avoir de redirection de port à faire. 

Le network host peut être utile dans certains cas, par exemple comme le container à accès à la carte réseau directement il va pouvoir analyser tout le traffic qui passe au sein de docker. 

> :warning:
>
> Faites d'ailleurs attention, comme le container est sur la carte réseau de la machine, le nom du container sera le même que la machine hote. 

### Le MACVLAN (ou le "c'est le bridge de VMware mais on a déjà utilisé le nom bridge donc on va lui donner un nom stylé")

Le MACVLAN est le dernier réseau que nous traiterons pendant ce TP (Il en existe encore mais ils sont moins essentiels que ceux que nous avons vu) et il est l'équivalent du bridge tel que nous l'entendons lorsque'on fait de la virtualisation avec VMware, c'est à dire que que si nous utilisons le MACVLAN sur un container il sera dans le même réseau que la machine hôte de celui-ci et les machine sur le même réseau pourrons le voir. 

Commençons par créer le network avec la commande suivante : 

```bash
docker network create -d macvlan --subnet <HOST_SUBNET> --gateway <SUBNET_GATEWAY> -o parent=<NETWORK_INTERFACE> --aux-address=<IP_TO_EXCLUDE> <NAME>
#Exemple, je suis dans mon réseau chez moi 192.168.0.0/24, l'ip de mon routeur est le 192.168.0.1, et la carte réseau de ma machine est ens33, et je veux exclure l'IP 192.168.0.2 car c'est l'IP de mon hote dans le réseau local et je veux l'appeller macdonalds
docker network create -d macvlan --subnet 192.168.0.0/24 --gateway 192.168.0.1 -o parent=ens33 --aux-address="192.168.0.2" macdonalds
```

Maintenant vous pouvez créer un container et l'assigner à ce réseau, vous devriez être en capacité de ping votre hôte depuis le container et inverssement, vous pouveze ping otre container depuis un CMD de votre hôte. 

## Exercice 7 

Vous allez mettre en place 2 réseaux, le premier va être un réseau user-defined bridge nommé "prod_network" en 172.25.0.0/16 dans lequel vous ferez tourner 2 3 container utilisant l'image "busybox" nommés comme bon vous semble. Le but est que les container puissent se ping entre eux au sein de leurs réseau en utilisant leurs nom.

Cela devrait ressembler à ça : 

![Network inspect](images/network_exo1.png)

Et vous allez ensuite créer un network MACVLAN appellé "vmware_like", qui ne distribuera des IPs que dans la deuxième partie de la range de votre réseau. 
Ex: le subnet de ma VM est le 192.168.0.0/24, je veux que les IPs soient distribués à partir de 192.168.0.128.

# Partie 5 : Le sacrosaint docker-compose

Nous allons maintenant attaquer les choses sérieuses. Il y a 2 types de personnes qui arrivent à cette partie, celles qui savent ce que le docker-compose et n'attendaient que ça, et celles qui ne savent pas ce que c'est et qui vont voir leurs vies chamboulées après ça. Docker compose qu'est ce que c'est ? C'est un module de Docker qui permet de lancer une stack de container d'un seul coup, avec leur réseaux associés, leurs volumes, leurs variables d'environnements et de tout supprimer (sauf les volumes) à l'arret de la stack. C'est extrêmement pratique quand on veut lancer un environnement de développement un peu complexe contenant par exemple un front, un back, une BDD, un Elasticsearch, etc... ou bien lancer une plateforme entière comme par exemple un Gitlab hébergé avec sa BDD et des runners pour faire l'intégration continue. 

### Installation du compose 

Pour télécharger Docker compose il va falloir exécuter les commandes suivante ([documentation](https://docs.docker.com/compose/install/standalone/)): 

```bash
curl -SL https://github.com/docker/compose/releases/download/v2.23.0/docker-compose-linux-x86_64 -o /usr/local/bin/docker-compose
chmod +x /usr/local/bin/docker-compose
sudo ln -s /usr/local/bin/docker-compose /usr/bin/docker-compose
#Test 
docker-compose --version
```

### Création du premier compose 

Pour la création de notre premier docker-compose, vous allez créer un nouveau dossier, vous rendre dedans et créer le fichier ```docker-compose.yml```, comme pour le Dockerfile, le nom docker-compose.yml est très important pour quand vous lancerez la commande.
Nous allons faire simple pour commencer en faisant une stack docker-compose qui lancera 2 containers, un sur nginx ouvert sur le port 8080 et un sur apache sur le port 8888. 

Voici la syntaxe du fichier : 

```yaml
version: "3"
services:
  nginx:
    image: nginx
    ports:
      - "8080:80"
    restart: always

  apache:
    image: httpd
    ports:
      - "8888:80"
```

Lancez la stack en faisant la commande ```docker-compose up -d```, "up" sert à lancer la stack ("down" pour au contraire l'arreter) et "-d" pour la lancer en mode détacher comme pour n'importe quel container docker que nous lançons en temps normal. Rendez vous sur votre navigateur et tapez l'ip de votre machine avec les ports que nous avons spécifiés. C'est censé tomber en marche ! 

Maintenant décomposons le docker-compose ligne par ligne (pour un service) : 
- ```version: "3"``` : c'est la version de compose que vous allez utiliser dans le fichier, certaines version possède plus ou moins de fonctionnalité, référez vous à la documentation pour en savoir plus. 
- ```services:``` : c'est la partie qui va contenir tous les containers
- ```nginx:``` : ca va être le nom de notre premier container et la section qui suit (grâce à l''indentation) est la configuration de ce container.
- ```image:``` : c'est l'image qui va être utilisée pour le container.
- ```ports:``` : C'est une liste qui va contenir tous les ports que nous voulons rediriger pour ce container.
- ```- "8080:80"``` : Comme nous l'avons vu précédemment c'est la redirection du port 8080 de l'hôte vers le poort 80 du container
- ```restart: always``` : Cela va permettre au container de se redémarrer à chaque fois qu'il est arrêté (à cause d'un rash par exemple)

Faisons un test, rentrez en CLI dans le container Nginx et arretez le process Nginx, faites un ```docker ps``` et voyez ce qu'il se passe. Que constatez vous ? 

Vous pouvez aussi faire du scalling, c'est à dire lancer plusieurs instance d'un même container, ce qui peut être utile pour par exemple avoir plusieurs runner pour de l'intégration continue, ou bien faire plusieurs back pour encaisser plus de charge. 

Pour se faire lors du lancer de la stack il faut rajouter l'option ```--scale <SERVICE_NAME>=<NUMBER>```.

## Exercice 8 

Vous allez faire une stack docker-compose contenant un service avec une image busybox, et au lancement de la stack vous allez en lancer 10 instances de ce container. 

### Compose avancé 

Nous allons voir des notions que nous avons vu précédemment, les volumes. Pour déclarer les volumes dans un docker-compose, il faut rajouter la section "volumes" qui sera au même niveau que la sections "services" (en terme d'indentation). Voici un exemple d'une stack mysql/phpmyadmin avec un volume sur la BDD pour que les données persistes après l'arrêt de la base. 

```yaml
version: '3'
 
services:
  db:
    image: mysql:5.7
    environment:
      MYSQL_ROOT_PASSWORD: password
      MYSQL_DATABASE: app_db
      MYSQL_USER: db_user
      MYSQL_PASSWORD: db_user_pass
    volumes:
      - dbdata:/var/lib/mysql

  phpmyadmin:
    image: phpmyadmin/phpmyadmin
    environment:
      PMA_HOST: db
      PMA_PORT: 3306
      PMA_ARBITRARY: 1
    restart: always
    ports:
      - 8081:80
volumes:
  dbdata:
```

Petite précisions : 

Pour injecter des variable d'envrionnement dans le container il faut utiliser cette instruction :
 ```yaml 
    environment:
      MYSQL_ROOT_PASSWORD: password
```

Pour mapper un volume il faut, comme avec un container docker normal faire un mapping du volume vers sa destionation dans le container, et aussi déclarer le volume dans la section "volumes" 
```yaml
    volumes:
      - dbdata:/var/lib/mysql
```
```yaml
volumes:
  dbdata:
```

Lancez la stack et connectez vous sur l''interface de phpmyadmin. Rajoutez des données en faisant un import de la base SQL de la partie sur les volumes. Une fois l'import terminé, arretez la stack docker-compose. 

Où est stocké le volume ? Quel est sont nom et à quoi celui-ci fait il référence ? 

## Exercice 9

Pour finir vous allez réaliser un dernier exercice plus compliqué sur dockier à l'aide du docker-compose. Le but va être de créer un stack entière qui se lancera d'un coup. 
Je vous laisse le choix, mais je recommande aux Infras de prendre la variante N°1 et aux Devs de prendre la variante N°2, mais vous pouvez être en Dev et prendre la variante N°1 et inversement. 

### Variante N°1 : Mise en place d'une instance Gitlab

### Variante N°2 : Mise en place d'une stack Wordpress

Afin d’illustrer le fonctionnement de docker-compose, nous allons construire un stack wordpress, c’est-à-dire une application WEB CMS et une base de données associée.
Notre stack tournera sur 2 services, dans un réseau bridge commun.
La base de données et les fichiers statiques html générés seront rendus persistants via 2 volumes.

#### service BDD 

Notre base de données fonctionnera sur une image mysql officielle. (Partez sur une mysql:5.7 pour pas avoir de soucis).

Il faudra veiller à rendre persistant le répertoire /var/lib/mysql afin de sauvegarder la bdd

Nous allons avoir besoin de passer 4 variables d’environnement liées à la connexion au SGBD, à définir vous-même (voir section du docker-compose avancé)

- MYSQL_ROOT_PASSWORD
- MYSQL_DATABASE
- MYSQL_USER
- MYSQL_PASSWORD

Le conteneur de base de données (ainsi que le serveur web) fonctionneront sur un réseau dédié dont vous choisirez le nom.

#### Service Wordpress

Le container wordpress sera instanciable directement via l’image officielle wordpress.

Il dépendra du conteneur de base de données (attendra que le conteneur bdd soit instancié avant d’être lancé)

Il écoutera sur le port 8080 

On devra sauvegarder les fichiers html statiques générés par le CMS qui seront dans /var/www/html

4 variables d’environnement à prévoir et à configurer de façon logique avec les variables du conteneur de bdd : 

- WORDPRESS_DB_HOST
- WORDPRESS_DB_USER
- WORDPRESS_DB_PASSWORD
- WORDPRESS_DB_NAME


#### Tips

Commencer par rédiger le docker-compose.yml, en définissant chaque service. Après définition des services, n’oubliez pas de définir globalement le réseau et les volumes que vous utilisez dans chacun des services.
Si vos services sont bien définis, lorsque vous allez tester la première connexion avec votre navigateur, vous allez être redirigés sur le script d’installation de wordpress comme ci-dessous. 

![Wordpress 1](images/wordpress1.png)

Remplir les informations d’administration de wordpress via le script d’installation

![Wordpress 2](images/wordpress2.png)

![Wordpress 3](images/wordpress3.png)

Puis après s’être logué, créer du contenu

![Wordpress 4](images/wordpress4.png)



##### ANNEXES

Le but va donc être de créer une image Docker contenant l'application [Petclinic](https://github.com/spring-petclinic/spring-framework-petclinic). Pour cela il va falloir effectuer les actions suivantes dans votre Dockerfile : 

- Utiliser une image contenant les binaires nécessaire pour compiler l'application (Les infos sur comment build l'application est dans le README du projet)
- Récupérer le projet sur git (Utiliser une image qui contient la CLI git ou bien l'installer)
- Build le projet à partir du code (si besoin)
- Exposer les ports de l'application (si besoin)
- Lancer l'application au démarrage du container

Pour cet exercice nous ne nous soucierons __UNIQUEMENT__ du front, pas besoin de lancer de back ou de BDD. 

Le résultat attendu est quelque chose semblable à ça : 

![Petclinic](images/petclinic.png)