# Table des matières
* Prérequis
* Fonctionnement
    * Image
    * Container
    * Dockerfile
    * Volume
* Commandes
    * docker image
    * docker volume
    * docker build
    * docker exec
    * docker logs
    * docker ps
    * docker stop
    * docker rm
    * docker inspect
    * docker pull


# Prérequis

La première étape d'avoir un système unix, pour les utilisateurs Windows, je vous conseille fortement d'installer un VM Linux de type Debian ou Ubuntu, je déconseille d'utiliser Docker Desktop for Windows qui utilise le WSL (vous pourrez rencontrez des problème plus tard dans le TP quand nous allons utiliser les volumes)

Une fois votre VM installé faites les commandes suivantes : 

```bash
apt update
apt install -y docker.io
```

Si vous utiliser docker avec un utilisateur différents de root, je vous conseille d'ajouter votre utilisateur au groupe docker pour vous éviter de devoir utiliser _sudo_ devant n'importe quelle commande docker. Pour cela tapez les commande suivante : 

```bash
sudo usermod -aG docker <USER>
```

Déconnectez et reconnectez votre session, votre utilisateur peut maintenant utiliser les commandes docker. 

Maintenant pour tester que Docker s'est bien installer utiliser la commande suivante : 

```bash
docker run hello-world
```

Si le résultat ressemble à quelque chose comme ça, c'est que tout est bon. 

![Docker Hello-world](images/docker-hello-world.png)

# Fonctionnement

Docker est une surcouche d'une fonctionnalité native de Linux qui s'appelle LXC, l'objectif de LXC est d'isoler un processus du reste du système pour qu'il ne puisse pas communiquer avec le reste des processus en cours d'exécution. De ce fait, on peut faire tourner plusieurs applications en parallèle sur la même machine. C'est de la virtualisation à l'intérieur d'un OS, mais sans avoir besoin de réinstaller la couche OS. 

![Virtualisation VS Docker](images/virtualisation.png)

Ces processus isolés sont appellés container. Un container va être en général un package qui contient une application ainsi que ses dépendences sous la forme d'un processus. 

Exemple : Nous développons un front en VueJS, nous allons le builder, et packager notre application avec un serveur web pour qu'il puisse être accessible, ainsi que toutes ses dépendences comme openssl pour permettre d'activer le SSL. Nous avons donc un container avec un seul processus, notre serveur web hébergeant notre front.  

Docker repose sur 3 composants essentiels, le Dockerfile, L'image Docker et le container.

![Composants Docker](images/servlet.jpeg)

## Image 

Une image Docker est un modèle à partir duquel sont instancié les containers Docker, pour faire un parallèle avec avec ce que vous connaissez :

- Pour les dev, c'est l'équivalent d'une classe, on peut instancier une infinité d'objet à partir de la même classe.
- Pour les infra, c'est l'équivalent d'un iso, on peut installer une infinité d'OS à partir du même iso, a la différence près que une fois l'iso installé l'OS sera indépendant de l'iso, dans Docker l'image et le container seront toujours liés.

## Container

Un container est un processus isolé du reste du système de l'hôte, qui fonctionne dans un environnement qui embarque toutes les dépendances du processus pour lui permettre de fonctionner correctement.

Exemples : 
- Pour les dev, on veut mettre a disposition notre code d'un application web. Le processus c'est notre code qui utilise un serveur web (ex: nginx), une de ses dépencdences va être le SSL pour lui permettre de faire du HTTPS.
- Pour les infra, on veut héberger une base de données. Le processus va être un notre moteur de BDD (ex: MySQL), une de ses dépencdences va être NFS-client car la BDD stocke ses données sur un serveur de stockage distant.

## Dockerfile

Le Dockerfile est le fichier utilisé pour créer un image, c'est la description de tout ce qui va être dans l'image Docker. Il va se baser sur une image existante et va ensuite ajouter ses commandes pour installer ses dépendances et définir le processus qui sera exécuter lors de l'exécution de l'image. 

## Volume 

A la base les containers Docker sont stateless, ce qui signifie qu'il ne sont pas censé contenir de data. Mais les cas d'usage de Docker ayant évolué nécéssitant de stocker des données, les conteneurs ont donc maintenant la possibilité d'être statefull, et cela au travers d'un composant appelé volume. 
Un volume est un espace de stockage attaché à un container lui permettant de conserver ses données une fois le container arreté ou supprimé.
Un volume est attaché à un container lors du lancement de celui-ci, et pour des raisons de sécurités il est possible d'attacher un volume en lecture seule, pour que le container ne puisse pas modifier les données présentent dans ce dernier.
Il existe 2 types de volume, les volumes "nommée" et les volumes "montés".

![Virtualisation VS Docker](images/docker-volume.png)

- Les volumes "nommés" : sont créés via la CLI de docker et sont stockés dans la zone de données de Docker.
- Les volumes "montés"" : sont des fichiers ou des dossiers de la machine hote, directement monté dans le container.

La différence principale est que les volume "nommés" sont manageable directement via la CLI de docker (avec la dommande _docker volume_ que nous verrons après).

> :warning:
>
> Si vous attachez un volume monté ou nommé dans un dossier non vide du container, le contenu du volume aura la priorité et supprimera ce qui devait se trouver dans le dossier du container.

# Commandes

Nous allons voir les commandes Docker les plus utilisées. Ceci est une liste non exhaustive, il existe plus de commandes mais les suivantes sont les principales, pour les autres, vous poouvez vous référer à la documentation officiel.

## docker run

La première commande de Docker et la plus importante est le ```docker run```, elle permet de lancer un container sur le server en utilisant un image Docker.

```bash
docker run -<OPTIONS> <NOM-IMAGE>
```
## docker image

La commande ```docker image <OPTION>``` sert à gérer les images docker présentent sur la machine, principalement les lister et supprimer.

```bash
#Lister les images
docker image ls
#Supprimer volume(s), on peut supprimer plusieurs image d'un coup, il suffit de séparer les ID par un espace
docker image rm <ID-IMAGE>
#Supprimer TOUTES les images
docker image prune
```

## docker volume

La commande ```docker volume <OPTION>``` sert à gérer les volumes docker. On peut donc créer, lister, supprimer les volumes présents sur la machine.

```bash
#Lister volumes
docker volume ls
#Créer volume
docker volume create <NOM-VOLUME>
#Supprimer volume(s), on peut supprimer plusieurs volumes d'un coup, il suffit de séparer les noms par un espace
docker volume rm <NOM-VOLUME>
#Supprimer TOUS les volumes
docker volume prune
```

## docker build
La commande ```docker build <OPTION> .``` sert à construire une image Docker à partir d'un Dockerfile, en lui donnant un nom ainsi qu'une version. La façon standard de l'utiliser et de se placer dans un dossier contenant un Dockerfile et lancer la commande suivante : 

```bash
docker build -t <NOM-IMAGE>:<VERSION> .
```

## docker exec
La commande ```docker exec <OPTION>``` sert à exécuter une commande à partir d'un container en fonctionnement, la plupart du temps elle est utilisée pour "rentrer" dans le container en CLI (comme un SSH à l'intérieur du container).

```bash
#Rentrer dans un container en bash
docker exec -it <CONTAINER-ID> bash 
#ou si bash n'existe pas dans le container 
docker exec -it <CONTAINER-ID> sh 
#Lancer une commande 
docker exec -it <CONTAINER-ID> <COMMANDE-BASH>
```
## docker logs
La commande ```docker logs <CONTAINER-ID>``` est une commande EXTRÊMEMENT utile quand il s'agit de débbuger des container Docker. La commande va nous afficher tous les logs émis par le container, ce qui peut nous aider par exemple a comprendre pourquoi un container crash en boucle.
```bash
#Affiche tous les logs d'un container (comme un "cat" d'un fichier de log)
docker logs <CONTAINER-ID>
#Affiche tous les logs d'un container en suivant l'output de log (comme avec un "tail -f" d'un fichier de log)
docker logs <CONTAINER-ID> -f
```
## docker ps
La commande ```docker ps <OPTION>``` est une commande très utile pour l'administration de Docker car elle sert à retourner la liste de tous les container sur la machine, running ou stopped.
```bash
#Affiche la liste des container running
docker ps 
#Affiche la liste de TOUS les containers sur la machine, peu importe leur statuts
docker ps -a
#Affiche la liste des ID des containers running, utile dans les scripts
docker ps -q
```
## docker stop
La commande ```docker stop <CONTAINER-ID>``` sert juste arreter un container en cours d'exécution. (Attention arreter, pas supprimer).
```bash
#Arreter un container
docker stop <CONTAINER-ID>|<CONTAINER-NAME>
```
## docker rm
La commande ```docker rm <CONTAINER-ID>``` sert a supprimer un container qui __n'est pas__ en cours d'exécution.
```bash
#Arreter un container
docker rm <CONTAINER-ID>|<CONTAINER-NAME>
```
## docker inspect
La commande ```docker inspect <CONTAINER-ID>``` va être utilisé pour "inspecter" un container pour récupérer sa configuration, comme par exemple les ports ouverts du container, son hostname, son IP dans le réseau Docker, si il a un stockage attaché et son path, etc...
```bash
#Récupérer les config d'un container
docker inspect <CONTAINER-ID>|<CONTAINER-NAME>
```
## docker pull
La commande ```docker pull <IMAGE>``` est une des commandes les plus basique de Docker, elle sert à télécharger une image depuis une registy (par défaut le docker hub). La commande n'est pas énormément utilisée, car quand on exécute la commande ```docker run XXXXXX``` si l'image n'est pas présente sur la machine, elle sera téléchargée directement, sans avoir besoin de pull l'image avant de faire le run.
```bash
#Télécharger une image docker
docker pull <IMAGE-NAME>:<TAG>
```
